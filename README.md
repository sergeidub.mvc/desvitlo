# DeSvitlo

Turn your additional smartphone into a sensor to monitor the availability of energy supply in your home.

Add phone numbers to which you would like to receive SMS notifications with the status of the presence of light. Make sure that your phone can send SMS to the specified numbers (the cost of sending SMS to the numbers specified by you is according to your operator's tariffs, the application does not charge any SMS fees).

Connect the smartphone to an outlet and leave it charging. As soon as the power supply is restored or turned off, the battery of your phone starts charging or stops charging, the application will notify you about this by sending SMS messages to the phone numbers you specified.

#### Installation
---

Install the dependencies and build the app.
Check that Flutter is installed on your machine and is ready to build by command `flutter doctor`
```sh
flutter pub get
flutter build apk --release
```

#### Links
---

Website: http://desvitlo.online/

DOU: https://dou.ua/forums/topic/40776/

E-mail: serhii(dot)dub[at]desvitlo.online
