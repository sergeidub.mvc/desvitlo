import 'package:desvitlo/mixins/form_helper.dart';
import 'package:desvitlo/mixins/popup_helper.dart';
import 'package:desvitlo/popups/prompt_alert.dart';
import 'package:desvitlo/utils/colors.dart';
import 'package:desvitlo/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:masked_text_field/masked_text_field.dart';
import 'package:permission_handler/permission_handler.dart';

class EditNumberPopup extends StatefulWidget {
  final Function(String) onSaved;
  final String? phoneNumber;

  const EditNumberPopup({
    super.key,
    required this.onSaved,
    this.phoneNumber,
  });

  @override
  State<StatefulWidget> createState() {
    return EditNumberPopupState();
  }
}

class EditNumberPopupState extends State<EditNumberPopup>
    with PopupHelper, ButtonHelper {
  final TextEditingController textFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.phoneNumber != null) {
      textFieldController.text = widget.phoneNumber!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: MaskedTextField(
              autofocus: true,
              textFieldController: textFieldController,
              escapeCharacter: '#',
              mask: "(###) ###-##-##",
              maxLength: 15,
              keyboardType: TextInputType.number,
              inputDecoration: const InputDecoration(
                hintText: "(050) 12-34-567",
                labelText: "Номер телефону",
              ),
              onChange: (String value) {},
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: buildButton(
              borderRadius: BorderRadius.circular(25),
              background: AppColors.buttonBlue,
              color: AppColors.white,
              text: 'Зберегти номер',
              width: 140,
              onPressed: () async {
                if (textFieldController.text.length == 15) {
                  widget.onSaved(textFieldController.text);
                }
                Navigator.of(context).pop();
                bool isSmsGranted = await Permission.sms.isGranted;
                if (!isSmsGranted) {
                  openPopup(
                    context: context,
                    child: AppPromptAlert(
                      text: Strings.smsPermissionTip,
                      fontSize: 18,
                      confirmText: 'Дозволити',
                      cancelText: 'Відмінити',
                      onConfirm: () async {
                        await Permission.sms.request();
                      },
                    ),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
