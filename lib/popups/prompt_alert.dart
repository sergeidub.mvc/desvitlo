import 'package:desvitlo/mixins/form_helper.dart';
import 'package:desvitlo/mixins/text_helper.dart';
import 'package:desvitlo/utils/colors.dart';
import 'package:flutter/material.dart';

class AppPromptAlert extends StatelessWidget with ButtonHelper, TextHelper {
  final Function() onConfirm;
  final String text;
  final String cancelText;
  final String confirmText;
  final double fontSize;

  const AppPromptAlert({
    super.key,
    required this.text,
    required this.onConfirm,
    this.cancelText = 'Ні',
    this.confirmText = 'Так',
    this.fontSize = 22,
  });

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: buildText(
              text: text,
              fontSize: fontSize,
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: buildButton(
                  borderRadius: BorderRadius.circular(10),
                  background: AppColors.white,
                  color: AppColors.buttonBlue,
                  text: cancelText,
                  onPressed: () async {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              const SizedBox(width: 20),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: buildButton(
                  borderRadius: BorderRadius.circular(10),
                  background: AppColors.buttonBlue,
                  color: AppColors.white,
                  text: confirmText,
                  onPressed: () async {
                    Navigator.of(context).pop();
                    onConfirm();
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
