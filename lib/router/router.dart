import 'package:desvitlo/bloc/core/bloc.dart';
import 'package:desvitlo/screen/helper.dart';
import 'package:desvitlo/screen/home.dart';
import 'package:desvitlo/screen/splash.dart';
import 'package:desvitlo/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppRouter {
  static const String routeSplash = 'splash';
  static const String routeHelper = 'helper';
  static const String routeHome = 'home';

  final CoreBloc _coreBloc = CoreBloc();

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {

      // Splash route
      // This is a main screen when user opens app

      case routeSplash:
        return MaterialPageRoute(
          builder: (_) => const SplashScreen(),
        );

      // Helper route
      // This screen contain instructions to setup app

      case routeHelper:
        return MaterialPageRoute(
          builder: (_) => const HelperScreen(),
        );

      // Home screen
      // This is a main control screen

      case routeHome:
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider.value(
                value: _coreBloc..add(const GetPhoneNumbersEvent()),
              ),
            ],
            child: const HomeScreen(),
          ),
        );

      // This route uses when requested screen not found
      // If you on this screen it is fatal error

      default:
        return MaterialPageRoute(
          builder: (_) => const Scaffold(
            body: Center(
              child: Text(
                'No route defined',
                style: TextStyle(
                  color: AppColors.black,
                ),
              ),
            ),
          ),
        );
    }
  }
}
