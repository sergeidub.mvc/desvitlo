import 'package:desvitlo/mixins/form_helper.dart';
import 'package:desvitlo/mixins/text_helper.dart';
import 'package:desvitlo/router/router.dart';
import 'package:desvitlo/utils/colors.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget with TextHelper, ButtonHelper {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.cerebralGrey,
        alignment: Alignment.center,
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: [
            _buildMainContent(context),
            _buildFooter(),
          ],
        ),
      ),
    );
  }

  Widget _buildMainContent(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buildLogo(),
        _buildTitle(),
        _buildDescription(),
        _buildStartButton(context),
      ],
    );
  }

  Widget _buildFooter() {
    return Positioned(
      bottom: 20,
      child: Column(
        children: [
          buildText(
            text: '#STANDWITHUKRAINE',
            alignment: Alignment.center,
            textAlign: TextAlign.center,
            fontSize: 24,
            margin: const EdgeInsets.symmetric(
              vertical: 20,
            ),
          ),
          buildText(
            text: 'Розробка: Сергій Дуб',
            alignment: Alignment.center,
            color: AppColors.millionGrey,
            fontSize: 12,
            margin: const EdgeInsets.only(
              bottom: 10,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLogo() {
    return Image.asset(
      'assets/images/icon.png',
      width: 64,
      height: 64,
    );
  }

  Widget _buildTitle() {
    return buildText(
      text: 'Де світло?',
      alignment: Alignment.center,
      textAlign: TextAlign.center,
      fontSize: 32,
      margin: const EdgeInsets.symmetric(
        vertical: 20,
      ),
    );
  }

  Widget _buildDescription() {
    return buildText(
      text:
          'Перетворіть свій додатковий смартфон в датчик для моніторингу наявності енергозабезпечення у Вас вдома',
      alignment: Alignment.center,
      textAlign: TextAlign.center,
      margin: const EdgeInsets.only(
        left: 20,
        right: 20,
      ),
    );
  }

  Widget _buildStartButton(BuildContext context) {
    return buildButton(
      text: 'Розпочати',
      background: AppColors.cyanite,
      color: AppColors.white,
      margin: const EdgeInsets.all(10),
      onPressed: () {
        Navigator.of(context).pushReplacementNamed(AppRouter.routeHelper);
      },
    );
  }
}
