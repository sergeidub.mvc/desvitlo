import 'package:desvitlo/mixins/form_helper.dart';
import 'package:desvitlo/mixins/text_helper.dart';
import 'package:desvitlo/router/router.dart';
import 'package:desvitlo/utils/colors.dart';
import 'package:desvitlo/utils/strings.dart';
import 'package:disable_battery_optimization/disable_battery_optimization.dart';
import 'package:flutter/material.dart';

class HelperScreen extends StatefulWidget {
  const HelperScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return HelperScreenState();
  }
}

class HelperScreenState extends State<HelperScreen>
    with TextHelper, ButtonHelper {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('DeSvitlo'),
      ),
      body: Container(
        color: AppColors.steam,
        alignment: Alignment.center,
        child: Column(
          children: [
            Expanded(
              child: _buildInstructions(),
            ),
            _buildFooter(context),
          ],
        ),
      ),
    );
  }

  Widget _buildInstructions() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        padding: const EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: 0,
        ),
        child: Column(
          children: [
            _buildStep(
              step: 1,
              text: Strings.helperStep1,
              extraWidget: buildButton(
                alignment: Alignment.centerLeft,
                width: double.infinity,
                text: 'Вимкнути оптимізацію',
                background: AppColors.green,
                color: AppColors.white,
                onPressed: () => _disableOptimization(),
              ),
            ),
            _buildStep(
              step: 2,
              text: Strings.helperStep2,
            ),
            _buildStep(
              step: 3,
              text: Strings.helperStep3,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildStep({
    required int step,
    required String text,
    Widget? extraWidget,
  }) {
    return Container(
      alignment: Alignment.topCenter,
      margin: const EdgeInsets.only(bottom: 20),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: AppColors.stoneColdSecondary,
        borderRadius: BorderRadius.circular(20),
        boxShadow: const [
          BoxShadow(
            color: AppColors.blackSecondary,
            offset: Offset(0, 0),
            blurRadius: 20,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColors.buttonBlue,
              borderRadius: BorderRadius.circular(100),
            ),
            child: buildText(
              text: step.toString(),
              fontWeight: FontWeight.bold,
              alignment: Alignment.center,
              textAlign: TextAlign.center,
              fontSize: 16,
              color: AppColors.white,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
              ),
              child: Column(
                children: [
                  buildText(
                    text: text,
                    color: AppColors.stoneCold,
                  ),
                  (extraWidget != null) ? extraWidget : Container(),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildFooter(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        color: AppColors.gravelFint,
        boxShadow: [
          BoxShadow(
            color: AppColors.millionGrey,
            blurRadius: 10,
            offset: Offset(2, 3),
          )
        ],
      ),
      child: buildButton(
        text: 'Продовжити',
        background: AppColors.buttonBlue,
        color: AppColors.white,
        margin: const EdgeInsets.all(10),
        onPressed: () {
          Navigator.of(context).pushReplacementNamed(AppRouter.routeHome);
        },
      ),
    );
  }

  void _disableOptimization() {
    DisableBatteryOptimization.showDisableBatteryOptimizationSettings();
  }
}
