import 'dart:async';
import 'package:battery_plus/battery_plus.dart';
import 'package:desvitlo/bloc/core/bloc.dart';
import 'package:desvitlo/mixins/form_helper.dart';
import 'package:desvitlo/mixins/popup_helper.dart';
import 'package:desvitlo/mixins/text_helper.dart';
import 'package:desvitlo/models/phone_number.dart';
import 'package:desvitlo/popups/prompt_alert.dart';
import 'package:desvitlo/router/router.dart';
import 'package:desvitlo/utils/colors.dart';
import 'package:desvitlo/utils/strings.dart';
import 'package:desvitlo/popups/edit_number.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sensors_plus/sensors_plus.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>
    with TextHelper, ButtonHelper, PopupHelper {
  double x = 0;
  double y = 0;
  double z = 0;

  final battery = Battery();
  late StreamSubscription<BatteryState> subscription;
  late bool monitoringEnabled = true;
  late bool readyToNotify = false;
  late Timer timer;
  late Timer magnetometerTimer;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      _checkBatteryState();
      _checkPermissions();
    });
    Future.delayed(const Duration(seconds: 1), () {
      _listenMagnetometerEvents();
      setState(() {
        readyToNotify = true;
      });
    });

    timer = Timer.periodic(const Duration(seconds: 1), (timer) async {
      await _checkPermissions();
    });
  }

  void _listenMagnetometerEvents() {
    magnetometerEvents.listen((MagnetometerEvent event) {
      if ((x - event.x).abs() > 5 ||
          (y - event.y).abs() > 5 ||
          (z - event.z).abs() > 5) {
        monitoringEnabled = false;
        _cancelMegnetometerTimer();
        magnetometerTimer =
            Timer.periodic(const Duration(seconds: 10), (timer) {
          monitoringEnabled = true;
          timer.cancel();
        });
      }
      x = event.x;
      y = event.y;
      z = event.z;
    });
  }

  Future<void> _checkPermissions() async {
    final bloc = BlocProvider.of<CoreBloc>(context);
    bloc.add(const CheckPermissionsEvent());
  }

  void _checkBatteryState() {
    subscription = battery.onBatteryStateChanged.listen((BatteryState state) {
      if (!monitoringEnabled) {
        return;
      }
      final bloc = BlocProvider.of<CoreBloc>(context);
      if (state == BatteryState.charging || state == BatteryState.full) {
        bloc.add(BatteryStateChangedEvent(
          isCharging: true,
          readyToNotify: readyToNotify,
        ));
      } else if (state == BatteryState.discharging) {
        bloc.add(BatteryStateChangedEvent(
          isCharging: false,
          readyToNotify: readyToNotify,
        ));
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
    timer.cancel();
    _cancelMegnetometerTimer();
  }

  void _cancelMegnetometerTimer() {
    try {
      magnetometerTimer.cancel();
    } catch (e) {
      // just catch erorr when cancel timer
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Де світло?'),
      ),
      body: Container(
        color: AppColors.steam,
        alignment: Alignment.center,
        child: Column(
          children: [
            Expanded(
              child: _buildMainContent(),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          openPopup(
            context: context,
            child: EditNumberPopup(
              phoneNumber: null,
              onSaved: (phoneNumber) {
                final bloc = BlocProvider.of<CoreBloc>(context);
                bloc.add(AddPhoneNumberEvent(PhoneNumber(
                  id: DateTime.now().millisecondsSinceEpoch.toString(),
                  displayPhone: phoneNumber,
                  formattedPhone: parsePhone(phoneNumber),
                )));
              },
            ),
          );
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), //
    );
  }

  Widget _buildMainContent() {
    return Container(
      padding: const EdgeInsets.only(
        top: 20,
        left: 20,
        right: 20,
        bottom: 0,
      ),
      child: Column(
        children: [
          _buildBatteryStatus(),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  _buildPermissionsStatus(),
                  _buildPhoneNumbers(),
                  const SizedBox(
                    height: 80,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBatteryStatus() {
    return Container(
      alignment: Alignment.center,
      child: BlocConsumer<CoreBloc, CoreState>(
        listenWhen: (previousState, state) {
          return true;
        },
        listener: (context, state) {},
        buildWhen: (context, state) {
          return state is BatteryStateChangedState;
        },
        builder: (context, state) {
          if (state is BatteryStateChangedState) {
            return buildFilledText(
              color: AppColors.stoneCold,
              backgroundColor: AppColors.stoneColdSecondary,
              fontWeight: FontWeight.bold,
              text: (state.isCharging)
                  ? Strings.powerStatusCharging
                  : Strings.powerStatusDischarging,
              alignment: Alignment.centerLeft,
              textAlign: TextAlign.start,
              margin: const EdgeInsets.only(bottom: 10),
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget _buildPermissionsStatus() {
    return Container(
      alignment: Alignment.center,
      child: BlocConsumer<CoreBloc, CoreState>(
        listenWhen: (previousState, state) {
          return true;
        },
        listener: (context, state) {},
        buildWhen: (context, state) {
          return state is CorePermissionsState;
        },
        builder: (context, state) {
          if (state is CorePermissionsState) {
            if (state.state == CorePermissionsState.stateHidden) {
              return Container();
            }
            return buildFilledText(
              color: _getAlertTextColor(state.state),
              backgroundColor: _getAlertBackgroundColor(state.state),
              text: _getAlertText(state.state),
              alignment: Alignment.centerLeft,
              textAlign: TextAlign.start,
              margin: const EdgeInsets.only(bottom: 10),
              onPressed: () async {
                openPopup(
                  context: context,
                  child: AppPromptAlert(
                    text: Strings.smsPermissionTip,
                    fontSize: 18,
                    confirmText: 'Дозволити',
                    cancelText: 'Відмінити',
                    onConfirm: () async {
                      await Permission.sms.request();
                    },
                  ),
                );
              },
            );
          }
          return Container();
        },
      ),
    );
  }

  Color _getAlertTextColor(int state) {
    if (state == CorePermissionsState.stateSuccess) {
      return AppColors.stoneCold;
    } else if (state == CorePermissionsState.stateWifiHelper) {
      return AppColors.stoneCold;
    } else {
      return AppColors.white;
    }
  }

  Color _getAlertBackgroundColor(int state) {
    if (state == CorePermissionsState.stateSuccess) {
      return Colors.green.shade200;
    } else if (state == CorePermissionsState.stateWifiHelper) {
      return AppColors.lightGrey;
    } else {
      return Colors.red.shade200;
    }
  }

  String _getAlertText(int state) {
    if (state == CorePermissionsState.stateSuccess) {
      return Strings.helperStep3;
    } else if (state == CorePermissionsState.stateWifiHelper) {
      return Strings.wifiPermissionTip;
    } else {
      return Strings.smsPermissionsDeniedTip;
    }
  }

  Widget _buildPhoneNumbers() {
    return Container(
      alignment: Alignment.center,
      child: BlocConsumer<CoreBloc, CoreState>(
        listenWhen: (previousState, state) {
          return true;
        },
        listener: (context, state) {},
        buildWhen: (context, state) {
          return state is PhoneNumbersState;
        },
        builder: (context, state) {
          if (state is PhoneNumbersState) {
            if (state.phoneNumbers.isEmpty) {
              return buildFilledText(
                text: Strings.phonesIsEmpty,
                fontSize: 18,
                color: AppColors.white,
              );
            }
            return Column(
              children: [
                buildText(
                  text: 'Номери для SMS оповіщень',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: AppColors.stoneCold,
                  margin: const EdgeInsets.only(
                    top: 30,
                    bottom: 10,
                  ),
                ),
                ...state.phoneNumbers.asMap().entries.map((entity) {
                  return _buildPhoneNumber(
                    phoneNumber: entity.value,
                  );
                })
              ],
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget _buildPhoneNumber({
    required PhoneNumber phoneNumber,
  }) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: AppColors.stoneColdSecondary,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Expanded(
            child: buildText(
              text: phoneNumber.displayPhone,
              fontSize: 22,
            ),
          ),
          buildIconButton(
            borderRadius: BorderRadius.circular(999),
            background: AppColors.buttonBlue,
            color: AppColors.white,
            icon: Icons.edit,
            size: 42,
            onPressed: () {
              openPopup(
                context: context,
                child: EditNumberPopup(
                  phoneNumber: phoneNumber.displayPhone,
                  onSaved: (updatedValue) {
                    final bloc = BlocProvider.of<CoreBloc>(context);
                    bloc.add(ReplacePhoneNumberEvent(PhoneNumber(
                      id: phoneNumber.id,
                      displayPhone: updatedValue,
                      formattedPhone: parsePhone(updatedValue),
                    )));
                  },
                ),
              );
            },
          ),
          const SizedBox(width: 10),
          buildIconButton(
            borderRadius: BorderRadius.circular(999),
            background: AppColors.buttonBlue,
            color: AppColors.white,
            icon: Icons.delete,
            size: 42,
            onPressed: () {
              openPopup(
                context: context,
                child: AppPromptAlert(
                  text: Strings.confirmDeleteNumber(
                    number: phoneNumber.displayPhone,
                  ),
                  confirmText: 'Підтверджую',
                  cancelText: 'Відмінити',
                  onConfirm: () {
                    final bloc = BlocProvider.of<CoreBloc>(context);
                    bloc.add(
                      DeletePhoneNumberEvent(
                        PhoneNumber(
                          id: phoneNumber.id,
                          displayPhone: phoneNumber.displayPhone,
                          formattedPhone: phoneNumber.formattedPhone,
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
