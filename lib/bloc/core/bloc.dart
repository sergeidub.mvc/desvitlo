import 'package:bloc/bloc.dart';
import 'package:desvitlo/mixins/form_helper.dart';
import 'package:desvitlo/models/phone_number.dart';
import 'package:desvitlo/repositories/phones_repository.dart';
import 'package:desvitlo/utils/strings.dart';
import 'package:equatable/equatable.dart';
import 'package:permission_handler/permission_handler.dart'
    as permission_handler;
import 'package:background_sms/background_sms.dart';

part 'event.dart';
part 'state.dart';

class CoreBloc extends Bloc<CoreEvent, CoreState> with ButtonHelper {
  final PhonesRepository phonesRepository = PhonesRepository();
  late bool isCharging = false;

  CoreBloc() : super(CoreInitState()) {
    on<BatteryStateChangedEvent>(_onBatteryStateChanged);
    on<GetPhoneNumbersEvent>(_onGetPhoneNumbers);
    on<AddPhoneNumberEvent>(_onAddPhoneNumber);
    on<ReplacePhoneNumberEvent>(_onReplacePhoneNumber);
    on<DeletePhoneNumberEvent>(_onDeletePhoneNumber);
    on<CheckPermissionsEvent>(_onCheckPermissions);
  }

  // -----------------------------------------
  // Event Handlers
  // -----------------------------------------

  _onBatteryStateChanged(
    BatteryStateChangedEvent event,
    Emitter<CoreState> emit,
  ) async {
    emit(CoreProcessingState());
    if (event.isCharging != isCharging) {
      List<PhoneNumber> phoneNumbers = await phonesRepository.get();
      try {
        _sendSmsNotifications(
          readyToNotify: event.readyToNotify,
          isCharging: event.isCharging,
          numbers: phoneNumbers,
        );
      } catch (e) {
        print(e.toString());
      }
      isCharging = event.isCharging;
    }
    emit(BatteryStateChangedState(isCharging: event.isCharging));
  }

  // getc phone number

  _onGetPhoneNumbers(
    GetPhoneNumbersEvent event,
    Emitter<CoreState> emit,
  ) async {
    emit(CoreProcessingState());
    List<PhoneNumber> phoneNumbers = await phonesRepository.get();
    emit(PhoneNumbersState(phoneNumbers: phoneNumbers));
  }

  // add phone number

  _onAddPhoneNumber(
    AddPhoneNumberEvent event,
    Emitter<CoreState> emit,
  ) async {
    emit(CoreProcessingState());
    List<PhoneNumber> phoneNumbers = await phonesRepository.get();
    if (phoneNumbers.length == 10) return;
    await phonesRepository.add(event.phoneNumber);
    phoneNumbers = await phonesRepository.get();
    emit(PhoneNumbersState(phoneNumbers: phoneNumbers));
  }

  // replace phone number

  _onReplacePhoneNumber(
    ReplacePhoneNumberEvent event,
    Emitter<CoreState> emit,
  ) async {
    emit(CoreProcessingState());
    await phonesRepository.replace(event.phoneNumber);
    List<PhoneNumber> phoneNumbers = await phonesRepository.get();
    emit(PhoneNumbersState(phoneNumbers: phoneNumbers));
  }

  // delete phone number

  _onDeletePhoneNumber(
    DeletePhoneNumberEvent event,
    Emitter<CoreState> emit,
  ) async {
    emit(CoreProcessingState());
    await phonesRepository.delete(event.phoneNumber);
    List<PhoneNumber> phoneNumbers = await phonesRepository.get();
    emit(PhoneNumbersState(phoneNumbers: phoneNumbers));
  }

  // check permissions

  _onCheckPermissions(
    CheckPermissionsEvent event,
    Emitter<CoreState> emit,
  ) async {
    emit(CoreProcessingState());

    List<PhoneNumber> phoneNumbers = await phonesRepository.get();

    if (phoneNumbers.isEmpty) {
      emit(const CorePermissionsState(
        state: CorePermissionsState.stateHidden,
      ));
      return;
    }

    bool isSmsGranted = await permission_handler.Permission.sms.isGranted;

    if (!isSmsGranted) {
      emit(const CorePermissionsState(
        state: CorePermissionsState.stateSmsError,
      ));
      return;
    }

    emit(const CorePermissionsState(
      state: CorePermissionsState.stateSuccess,
    ));
  }

  // -----------------------------------------
  // Functions
  // -----------------------------------------

  Future<void> _sendSmsNotifications({
    required bool readyToNotify,
    required bool isCharging,
    required List<PhoneNumber> numbers,
    int attempt = 0,
  }) async {
    if (readyToNotify) {
      for (var number in numbers) {
        String recipient = parsePhone(
          number.formattedPhone,
          prefix: '+38',
        );
        String message = (isCharging)
            ? Strings.smsMessagePowerOn()
            : Strings.smsMessagePowerOff();
        SmsStatus status = await BackgroundSms.sendMessage(
          phoneNumber: recipient,
          message: message,
        );

        if (status == SmsStatus.failed && attempt < 3) {
          await Future.delayed(const Duration(seconds: 1));
          _sendSmsNotifications(
            readyToNotify: readyToNotify,
            isCharging: isCharging,
            numbers: numbers,
            attempt: attempt + 1,
          );
          return;
        }
      }
    }
  }
}
