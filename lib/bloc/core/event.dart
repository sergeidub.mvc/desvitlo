part of 'bloc.dart';

abstract class CoreEvent<T> extends Equatable {
  const CoreEvent();
}

/////////////////////////////////////////////////////////////

class BatteryStateChangedEvent extends CoreEvent {
  final bool isCharging;
  final bool readyToNotify;

  const BatteryStateChangedEvent({
    required this.isCharging,
    required this.readyToNotify,
  });

  @override
  List<Object?> get props => [isCharging, readyToNotify];
}

// get phone numbers

class GetPhoneNumbersEvent extends CoreEvent {
  const GetPhoneNumbersEvent();

  @override
  List<Object?> get props => [];
}

// add phone number

class AddPhoneNumberEvent extends CoreEvent {
  final PhoneNumber phoneNumber;
  const AddPhoneNumberEvent(this.phoneNumber);

  @override
  List<Object?> get props => [phoneNumber];
}

// replace phone number

class ReplacePhoneNumberEvent extends CoreEvent {
  final PhoneNumber phoneNumber;
  const ReplacePhoneNumberEvent(this.phoneNumber);

  @override
  List<Object?> get props => [phoneNumber];
}

// delete phone number

class DeletePhoneNumberEvent extends CoreEvent {
  final PhoneNumber phoneNumber;
  const DeletePhoneNumberEvent(this.phoneNumber);

  @override
  List<Object?> get props => [phoneNumber];
}

// check permissions

class CheckPermissionsEvent extends CoreEvent {
  const CheckPermissionsEvent();

  @override
  List<Object?> get props => [];
}
