part of 'bloc.dart';

abstract class CoreState extends Equatable {
  const CoreState();

  @override
  List<Object> get props => [];
}

class CoreInitState extends CoreState {}

class CoreProcessingState extends CoreState {}

// Battery state

class BatteryStateChangedState extends CoreState {
  final bool isCharging;

  const BatteryStateChangedState({
    required this.isCharging,
  });

  @override
  List<Object> get props => [isCharging];
}

// Permissions state

class CorePermissionsState extends CoreState {
  static const int stateHidden = 0;
  static const int stateSuccess = 1;
  static const int stateSmsError = 2;
  static const int stateWifiHelper = 3;

  final int state;

  const CorePermissionsState({
    required this.state,
  });

  @override
  List<Object> get props => [state];
}

// Phone numbers

class PhoneNumbersState extends CoreState {
  final List<PhoneNumber> phoneNumbers;

  const PhoneNumbersState({
    required this.phoneNumbers,
  });

  @override
  List<Object> get props => [phoneNumbers];
}
