import 'package:desvitlo/models/phone_number.dart';
import 'package:desvitlo/utils/storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PhonesRepository {
  SharedPreferences? prefs;

  Future<SharedPreferences> _getInstance() async {
    prefs ??= await SharedPreferences.getInstance();
    return prefs!;
  }

  Future<List<PhoneNumber>> get() async {
    SharedPreferences instance = await _getInstance();
    List<String>? items = instance.getStringList(Prefs.keyPhones);
    if (items != null) {
      return items.asMap().entries.map(
        (item) {
          return PhoneNumber.fromString(item.value);
        },
      ).toList();
    }
    return [];
  }

  Future<void> add(PhoneNumber model) async {
    SharedPreferences instance = await _getInstance();
    List<PhoneNumber> items = await get();

    items.add(model);
    await instance.setStringList(
      Prefs.keyPhones,
      items.asMap().entries.map((item) => item.value.toString()).toList(),
    );
  }

  Future<void> delete(PhoneNumber model) async {
    SharedPreferences instance = await _getInstance();
    List<PhoneNumber> items = await get();

    await instance.setStringList(
      Prefs.keyPhones,
      deleteItemFromList(
        model: model,
        items: items,
        runtimeType: runtimeType,
      ).asMap().entries.map((entity) => entity.value.toString()).toList(),
    );
  }

  Future<void> replace(PhoneNumber model) async {
    SharedPreferences instance = await _getInstance();
    List<PhoneNumber> items = await get();
    for (var item in items) {
      if (item.id == model.id) {
        await instance.setStringList(
          Prefs.keyPhones,
          items.asMap().entries.map((item) {
            if (item.value.id == model.id) {
              return model.toString();
            }
            return item.value.toString();
          }).toList(),
        );
        return;
      }
    }

    throw Exception('Item not exists: $model');
  }

  Future<void> reset() async {
    SharedPreferences instance = await _getInstance();
    await instance.setStringList(Prefs.keyPhones, []);
  }

  void throwIfExists({
    required PhoneNumber model,
    required List<PhoneNumber> items,
    required Type runtimeType,
  }) {
    for (var item in items) {
      if (item.id == model.id) {
        throw Exception('Repository exixts');
      }
    }
  }

  List<PhoneNumber> deleteItemFromList({
    required PhoneNumber model,
    required List<PhoneNumber> items,
    required Type runtimeType,
  }) {
    List<PhoneNumber> filtered = [];
    for (var item in items) {
      if (item.id == model.id) continue;
      filtered.add(item);
    }
    if (items.length == filtered.length) {
      throw Exception('Item not found: $runtimeType');
    }
    return filtered;
  }
}
