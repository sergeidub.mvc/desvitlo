import 'package:desvitlo/utils/colors.dart';
import 'package:flutter/material.dart';

class TextHelper {
  Widget buildText({
    required String text,
    Alignment alignment = Alignment.centerLeft,
    TextAlign textAlign = TextAlign.start,
    double fontSize = 16,
    Color color = AppColors.black,
    FontWeight fontWeight = FontWeight.normal,
    EdgeInsets margin = EdgeInsets.zero,
  }) {
    return Container(
      margin: margin,
      alignment: alignment,
      child: Text(
        text,
        softWrap: true,
        textAlign: textAlign,
        style: TextStyle(
          color: color,
          fontSize: fontSize,
          fontWeight: fontWeight,
        ),
      ),
    );
  }

  Widget buildFilledText({
    required String text,
    Alignment alignment = Alignment.centerLeft,
    TextAlign textAlign = TextAlign.start,
    double fontSize = 16,
    Color color = AppColors.black,
    Color backgroundColor = AppColors.cyanite,
    FontWeight fontWeight = FontWeight.normal,
    EdgeInsets margin = EdgeInsets.zero,
    Function()? onPressed,
    double? height,
  }) {
    return InkWell(
      onTap: () {
        if (onPressed != null) {
          onPressed();
        }
      },
      child: Container(
        margin: margin,
        height: height,
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: AppColors.blackSecondary,
              offset: Offset(0, 0),
              blurRadius: 20,
            ),
          ],
        ),
        padding: const EdgeInsets.all(10),
        child: buildText(
          text: text,
          alignment: alignment,
          textAlign: textAlign,
          fontSize: fontSize,
          color: color,
          fontWeight: fontWeight,
        ),
      ),
    );
  }
}
