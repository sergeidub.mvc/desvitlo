import 'package:desvitlo/utils/colors.dart';
import 'package:flutter/material.dart';

class ButtonHelper {
  Widget buildButton({
    required String text,
    required Function() onPressed,
    Alignment alignment = Alignment.center,
    TextAlign textAlign = TextAlign.center,
    Color color = AppColors.cyanite,
    Color background = AppColors.transparent,
    EdgeInsets margin = EdgeInsets.zero,
    double? width,
    BorderRadius? borderRadius,
  }) {
    return Container(
      width: width,
      margin: margin,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        borderRadius: borderRadius,
      ),
      alignment: alignment,
      child: TextButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(background),
          shadowColor: MaterialStateProperty.all<Color>(background),
        ),
        onPressed: onPressed,
        child: Text(
          text,
          textAlign: textAlign,
          style: TextStyle(
            color: color,
          ),
        ),
      ),
    );
  }

  Widget buildIconButton({
    required IconData icon,
    required Function() onPressed,
    Alignment alignment = Alignment.center,
    TextAlign textAlign = TextAlign.center,
    Color color = AppColors.cyanite,
    Color background = AppColors.transparent,
    EdgeInsets margin = EdgeInsets.zero,
    double? size,
    BorderRadius? borderRadius,
  }) {
    return Container(
      width: size,
      height: size,
      margin: margin,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        color: background,
        borderRadius: borderRadius,
      ),
      alignment: alignment,
      child: TextButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(background),
          shadowColor: MaterialStateProperty.all<Color>(background),
        ),
        onPressed: onPressed,
        child: Icon(
          icon,
          color: color,
        ),
      ),
    );
  }

  String parsePhone(
    String phone, {
    String prefix = '',
  }) {
    return prefix + phone.replaceAll(RegExp(r"\D"), "");
  }
}
