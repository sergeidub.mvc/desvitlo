import 'package:flutter/material.dart';

class PopupHelper {
  void openPopup({
    required BuildContext context,
    required Widget child,
  }) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: child,
        );
      },
    );
  }
}
