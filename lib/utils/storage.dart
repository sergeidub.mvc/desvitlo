import 'package:shared_preferences/shared_preferences.dart';

class AppStorage {
  Future<void> set({
    required String key,
    required String value,
  }) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  Future<String?> get({
    required String key,
  }) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  Future<bool> delete({
    required String key,
  }) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.remove(key);
  }
}

class Prefs {
  static const String keyPhones = 'keyPhones';
}
