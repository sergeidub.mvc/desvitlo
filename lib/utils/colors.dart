import 'package:flutter/material.dart';

class AppColors {
  static const Color cyanite = Color(0xFF01B3E3);
  static const Color cerebralGrey = Color(0xFFCCCCCC);
  static const Color lightGrey = Color(0xFFCACACA);
  static const Color steam = Color(0xFFDDDDDD);
  static const Color gravelFint = Color(0xFFBBBBBB);
  static const Color stoneCold = Color(0xFF555555);
  static const Color stoneColdSecondary = Color(0x11555555);
  static const Color millionGrey = Color(0xFF999999);
  static const Color blackSecondary = Color(0x11000000);
  static const Color buttonBlue = Color(0xFF2196F3);
  static const Color green = Color(0xFF4CAF50);
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color transparent = Color(0x00000000);
}
