import 'dart:convert';

class PhoneNumber {
  final String id;
  final String displayPhone;
  final String formattedPhone;

  PhoneNumber({
    required this.id,
    required this.displayPhone,
    required this.formattedPhone,
  });

  factory PhoneNumber.fromString(String jsonString) {
    Map object = json.decode(jsonString);
    return PhoneNumber(
      id: object['id'],
      displayPhone: object['displayPhone'],
      formattedPhone: object['formattedPhone'],
    );
  }

  PhoneNumber copyWith({
    String? displayPhone,
    String? formattedPhone,
  }) {
    return PhoneNumber(
      id: id,
      displayPhone: displayPhone ?? this.displayPhone,
      formattedPhone: formattedPhone ?? this.formattedPhone,
    );
  }

  Map<String, String> asMap() {
    return {
      'id': id,
      'displayPhone': displayPhone,
      'formattedPhone': formattedPhone,
    };
  }

  @override
  String toString() {
    return json.encode(asMap());
  }
}
