import 'package:desvitlo/router/router.dart';
import 'package:desvitlo/utils/colors.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final AppRouter router = AppRouter();

  MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeSvitlo',
      theme: ThemeData(
        primaryColor: AppColors.cyanite,
        appBarTheme: const AppBarTheme(
          backgroundColor: AppColors.cyanite,
        ),
        scaffoldBackgroundColor: AppColors.white,
      ),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: router.onGenerateRoute,
      initialRoute: AppRouter.routeSplash,
    );
  }
}
